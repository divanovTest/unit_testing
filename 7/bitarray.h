#ifndef BITARRAY_H
#define BITARRAY_H

#include <vector>
#include <algorithm>
#include <cmath>

void int_to_vec(unsigned long long int initVal, std::vector<unsigned int> &vec)
{
	vec.clear();
	while(initVal > 0)
	{
		initVal % 2 == 0 ? vec.push_back(0) : vec.push_back(1);
		initVal /= 2;
	}
}

class BitArray{
public:
	BitArray (unsigned int initVal) : length(32)//инициализирует биты числом initVal
	{
		int_to_vec(initVal, this->vec);
		int delta =  32 - this->vec.size();
		//std::reverse(this->vec.begin(), this->vec.end());
		while (delta--)
			this->vec.push_back(0);
		std::reverse(this->vec.begin(), this->vec.end());
	}

	BitArray(BitArray& b)
	{
		this->length = b.length;
		this->vec.clear();
		for (int i = 0; i < b.length; ++i)
		{
			this->vec.push_back(b.vec[i]);
		}
	}

	unsigned long long int vec_to_int()
	{
		int st = 0, res = 0;
		for_each(this->vec.rbegin(), this->vec.rend(), [&res, &st](int n){
			res += (n * pow(2, st)); 
			++st;
		});
		return res;
	}

	void printVec()
	{
		for (std::vector<unsigned int>::const_iterator i = this->vec.begin(); i != this->vec.end(); ++i)
   			std::cout << *i;
   		std::cout << std::endl;
	}

	void SetLength (int len = 32)//изменяет длину массива, len – длина в битах. По умолчанию длина равна 32
	{
		int delta = len - length;
		std::reverse(this->vec.begin(), this->vec.end());
		while (delta-- >0)
			this->vec.push_back(0);
		std::reverse(this->vec.begin(), this->vec.end());
		length = len;
	}

	int GetLength()//возвращает длину массива.
	{
		return length;
	}

	void SetBit (int pos, bool value)//выставляет бит pos в значение value
	{
		vec[pos] = value;
	}

	bool GetBit (int pos)//возвращает значение бита pos
	{
		return (bool)vec[pos];
	}

	BitArray& operator<<(int num) //сдвиг влево
	{
		while (num-- > 0)
		{
			this->vec.erase(this->vec.begin());
			this->vec.push_back(0);
		}
		return *this;
	}

	BitArray& operator>>(int num) //сдвиг вправо
	{
		std::reverse(this->vec.begin(), this->vec.end());
		while (num-- > 0)
		{
			this->vec.erase(this->vec.begin());
			this->vec.push_back(0);
		}
		std::reverse(this->vec.begin(), this->vec.end());
		return *this;
	}

	friend BitArray& operator ~(BitArray &ba)
	{
		for_each(ba.vec.begin(), ba.vec.end(), [](unsigned int &n){ n == 0 ? n = 1 : n = 0;});
		return ba;
	}

	friend BitArray& operator &(BitArray &bal, BitArray &bar)
	{
		std::vector<unsigned int>::reverse_iterator baliter = bal.vec.rbegin();
		std::vector<unsigned int>::reverse_iterator bariter = bar.vec.rbegin();
		while(baliter != bal.vec.rend() && bariter != bar.vec.rend())
		{
			*baliter = (*baliter == 1 && *bariter == 1) ? 1 : 0 ;
			++bariter;
			++baliter;
		}
		return bal;
	}

	friend BitArray& operator |(BitArray &bal, BitArray &bar)
	{
		std::vector<unsigned int>::reverse_iterator baliter = bal.vec.rbegin();
		std::vector<unsigned int>::reverse_iterator bariter = bar.vec.rbegin();
		while(baliter != bal.vec.rend() && bariter != bar.vec.rend())
		{
			*baliter = (*baliter == 0 && *bariter == 0) ? 0 : 1;
			++bariter;
			++baliter;
		}
		return bal;
	}

	BitArray& operator=(BitArray & ba)
	{
		this->length = ba.length;
		this->vec = ba.vec;
		return *this;
	}

	friend std::ostream& operator<< (std::ostream & stream, BitArray & obj)//вывод в поток (при выводе группировать по 8 бит)
	{
		int temp =1;
		for_each(obj.vec.begin(), obj.vec.end(), [&stream, &temp](unsigned int &i){stream << i << (temp % 8 == 0 ? " " : ""); ++temp;});
		stream << " " ;
	}

private:
	unsigned int length;
	std::vector<unsigned int> vec;
};

#endif //BITARRAY_H