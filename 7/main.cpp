#include <iostream>
#include "bitarray.h"
#include <gtest/gtest.h>

using namespace std;

TEST(BitArray, BitArrayClassTest) 
{
	BitArray ba(20);
	ASSERT_EQ(20, ba.vec_to_int());
	ba = ba << 4;
	ASSERT_EQ(320, ba.vec_to_int());
	ba = ba >> 4;
	ASSERT_EQ(20, ba.vec_to_int());
	BitArray baCopy(ba);
	ASSERT_EQ(20, baCopy.vec_to_int());
	baCopy.SetLength(12);
	ASSERT_EQ(12, baCopy.GetLength());
	baCopy.SetBit(31, 1);
   	ASSERT_EQ(21, baCopy.vec_to_int());
   	ASSERT_EQ(1, baCopy.GetBit(31));
   	baCopy = ~baCopy;
   	ASSERT_EQ(18446744071562067968, baCopy.vec_to_int());
   	baCopy = baCopy & ~baCopy;
   	ASSERT_EQ(20, baCopy.vec_to_int());
   	baCopy = baCopy | ~ba;
   	ASSERT_EQ(18446744071562067968, baCopy.vec_to_int());
}

int main(int argc, char **argv)
{
	testing::InitGoogleTest(&argc, argv);
   	return RUN_ALL_TESTS();
	//return 0;
}